from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from .models import Album
# from django.template import loader
from django.shortcuts import render
from django.http import Http404

# Create your views here.

def index(request):
	albums = Album.objects.all()
	context = {
		'albums' : albums
	}

	return render(request, 'music/index.html', context)

def detail(request, album_id):

	# try :
	# 	album = Album.objects.get(id=album_id)
	# except Album.DoesNotExist:
	# 	raise Http404('Sorry that doesnot exist')	

	album = get_object_or_404(Album, id = album_id)

	return render(request, 'music/detail.html', { 'album' : album });
	# return HttpResponse('<h2>Details for an album' + str(album_id) + '</h2>')