from django.conf.urls import url
from .import views

app_name = 'music'

urlpatterns = [
	#This should direct to the music home/index
	url(r'^$', views.index, name='index'),

	#This one is for detail => /music/231
	url(r'^(?P<album_id>[0-9]+)/$', views.detail, name='detail'),

	#This one is for detail => /music/231/favourite
	url(r'^(?P<album_id>[0-9]+)/favourite/$', views.detail, name='favourite')
]